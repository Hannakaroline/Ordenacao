
import java.io.FileNotFoundException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author hanna
 */
public class QuickSort extends GenericSort {

    public QuickSort(String path) throws FileNotFoundException {
        super(path);
    }

    public QuickSort(int[] array) {
        super(array);
    }

    @Override
    public void sortArray() {
        long start = System.currentTimeMillis();
        quickSort(0, vet.length - 1, vet);
        long finish = System.currentTimeMillis();
        time = finish - start;
    }

    private void swap(int i, int j) {
        int troca = vet[i];
        vet[i] = vet[j];
        vet[j] = troca;
    }

    private int partition(int p, int r, int[] v) {
        int x, i, j;
        x = v[r];
        i = p - 1;
        for (j = p; j <= r - 1; j++) {
            if (v[j] >= x) {
                i = i + 1;
                swap(i, j);
            }
        }
        swap(i + 1, r);
        return i + 1;
    }

    private void quickSort(int p, int r, int[] v) {
        int q;
        if (p < r) {
            q = partition(p, r, v);
            quickSort(p, q - 1, v);
            quickSort(q + 1, r, v);
        }
    }

}
