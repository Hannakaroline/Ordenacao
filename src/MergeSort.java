
import java.io.FileNotFoundException;

public class MergeSort extends GenericSort {

    public MergeSort(String path) throws FileNotFoundException {
        super(path);
    }

    public MergeSort(int[] array) {
        super(array);
    }

    @Override
    public void sortArray() {
        long start = System.currentTimeMillis();
        mergeSort(0, vet.length, vet);
        long finish = System.currentTimeMillis();
        time = finish - start;
    }

    private void merge(int p, int q, int r, int[] v) {
        int i, j, k;
        int[] w = new int[v.length];
        i = p;
        j = q;
        k = 0;

        while (i < q && j < r) {
            if (v[i] > v[j]) {
                w[k] = v[i];
                i++;
            } else {
                w[k] = v[j];
                j++;
            }
            k++;
        }
        while (i < q) {
            w[k] = v[i];
            i++;
            k++;
        }
        while (j < r) {
            w[k] = v[j];
            j++;
            k++;
        }
        for (i = p; i < r; i++) {
            v[i] = w[i - p];
        }
    }

    private void mergeSort(int p, int r, int[] v) {
        int q;
        if (p < r - 1) {
            q = (p + r) / 2;
            mergeSort(p, q, v);
            mergeSort(q, r, v);
            merge(p, q, r, v);
        }
    }
}
