/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author hanna
 */
public class HeapMaximo {

    private int[] h;
    private int numElementos;
    private int j;

    public HeapMaximo(int[] vet) {
        h = vet;
        numElementos = vet.length;
    }

    void construirHeap2() {
        int ultimo = (int) (Math.floor((double) numElementos / 2)) - 1;
        for (int i = ultimo; i >= 0; i--) {
            descer(i);
        }
    }

    void trocaElementos(int i, int j) {
        int aux = h[i];
        h[i] = h[j];
        h[j] = aux;
    }

    void decNumElementos() {
        numElementos--;
    }

    void descer(int indice) {
        j = indice * 2;
        if (j < numElementos) {
            if (j < numElementos - 1) {
                if (h[j + 1] < h[j]) {
                    j++;
                }
            }
            if (h[indice] > h[j]) {
                int aux = h[indice];
                h[indice] = h[j];
                h[j] = aux;
                descer(j);
            }
        }
    }
}
