
import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Principal {

    public static void main(String[] args) {
        try {

            GenericSort generalSort = new HeapSort(args[0]);
            

            generalSort.sortArray();
            System.out.println(generalSort.toString());
            System.out.println("tempo: " + generalSort.time +" " +"ms");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
