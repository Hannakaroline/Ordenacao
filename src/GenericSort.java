
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public abstract class GenericSort implements SortingInterface {

    protected long time = 0;
    protected int vet[];

    public GenericSort(String path) throws FileNotFoundException {
        File file = new File(path);
        Scanner scanner = new Scanner(file);
        int linhas = scanner.nextInt();
        this.vet = new int[linhas];
        for (int i = 0; i < linhas; i++) {
            this.vet[i] = scanner.nextInt();
        }
        sortArray();
    }

    public GenericSort(int[] array) {
        this.vet = array.clone();
    }

    public void imprimir() {
        for (int i = 0; i < vet.length; i++) {
            System.out.println(vet[i]);
        }
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < vet.length; i++) {
            stringBuilder.append(vet[i]);
            if (i < vet.length - 1) {
                stringBuilder.append(" ");
            }
        }
        stringBuilder.append("\n");
        return stringBuilder.toString();
    }
}
