
import java.io.FileNotFoundException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author hanna
 */
public class BubbleSort extends GenericSort {

    public BubbleSort(String path) throws FileNotFoundException {
        super(path);
    }

    public BubbleSort(int[] array) {
        super(array);
    }

    @Override
    public void sortArray() {
        long start = System.currentTimeMillis();
        bubbleSort( vet.length, vet);
        long finish = System.currentTimeMillis();
        time = finish - start;
    }

    private void bubbleSort(int n, int[] v) {
        int i, j;
        for (i = n - 1; i > 0; i--) {
            for (j = 0; j < i; j++) {
                if (v[j] < v[j + 1]) {
                    swap(j, j + 1);
                }
            }
        }

    }

    private void swap(int j, int i) {
        int troca = vet[j];
        vet[j] = vet[j+1];
        vet[j+1] = troca;
        
    }

}
