
import java.io.FileNotFoundException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author hanna
 */
public class CountingSort extends GenericSort {

    private int maxVxalue;
    private int maxValue;

    public CountingSort(String path) throws FileNotFoundException {
        super(path);
    }

    public CountingSort(int[] array) {
        super(array);
    }

    @Override
    public void sortArray() {
        long start = System.currentTimeMillis();
        countingSort(vet, vet.length);
        long finish = System.currentTimeMillis();
        time = finish - start;
    }

    public void sort(int maxValue) {
        this.maxValue = maxValue;
    }

   private void countingSort(int[] v, int n) {
        int[] aux = new int[n + 1];
        int[] resp = new int[v.length];
      
        for (int i = 0; i < v.length; i++) {
            aux[v[i]]++;
        }

        for (int i = 1; i < aux.length; i++) {
            aux[i] += aux[i - 1];
        }

        for (int i = v.length - 1; i >= 0; i--) {
            resp[aux[v[i]] - 1] = v[i];
            aux[v[i]]--;
        }
        for (int i = 0; i < v.length; i++) {
            v[i] = resp[i];
        }
    }
}
