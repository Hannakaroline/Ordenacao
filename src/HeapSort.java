
import java.io.FileNotFoundException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author hanna
 */
public class HeapSort extends GenericSort {

     public HeapSort(String path) throws FileNotFoundException {
        super(path);
    }
    
    public HeapSort(int[] array) {
        super(array);
    }

    @Override
    public void sortArray() {
        long start = System.currentTimeMillis();
        heapSort(vet);
        long finish = System.currentTimeMillis();
        time = finish - start;
    }
    
    public void heapSort(int[]vet){
        HeapMaximo h = new HeapMaximo(vet);
        h.construirHeap2();
        for(int i = vet.length - 1 ;i > 0;i--){
            h.trocaElementos(0,i);
            h.decNumElementos();
            h.descer(0);
        }
    }
}
