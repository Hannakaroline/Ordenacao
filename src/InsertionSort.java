
import java.io.FileNotFoundException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author hanna
 */
public class InsertionSort extends GenericSort {

    public InsertionSort(String path) throws FileNotFoundException {
        super(path);
    }

    public InsertionSort(int[] array) {
        super(array);
    }

    @Override
    public void sortArray() {
        long start = System.currentTimeMillis();
        insertionSort(vet.length, vet);
        long finish = System.currentTimeMillis();
        time = finish - start;
    }

    public void insertionSort(int n, int[] v) {
        int i, j, x;
        for (i = 1; i < n; i++) {
            x = v[i];
            for (j = i - 1; j >= 0 && v[j] < x; j--) 
                v[j + 1] = v[j];
            
            v[j + 1] = x;
        }
    }
}
