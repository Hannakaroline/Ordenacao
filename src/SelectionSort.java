
import java.io.FileNotFoundException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author hanna
 */
public class SelectionSort extends GenericSort {

    public SelectionSort(String path) throws FileNotFoundException {
        super(path);
    }

    public SelectionSort(int[] array) {
        super(array);
    }

    @Override
    public void sortArray() {
        long start = System.currentTimeMillis();
        selectionSort(vet.length, vet);
        long finish = System.currentTimeMillis();
        time = finish - start;
    }

    private void selectionSort(int n, int[] v) {
        int i, j, min;
        for (i = 0; i < n - 1; i++) {
            min = i;
            for (j = i + 1; j < n; j++) {
                if (v[j] < v[min]) {
                    min = j;
                }
                swap(j,min);
            }
        }
    }

    private void swap(int j, int min) {
        int troca = vet[j];
        vet[j] = vet[min];
        vet[min] = troca;

    }
}
